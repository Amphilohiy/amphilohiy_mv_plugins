var peg = require("pegjs");
var fs = require("fs");

var grammar = fs.readFileSync('./coffee/PDL/dsl.pegjs', "utf8");

var parser = peg.generate(grammar);

console.log(JSON.stringify(parser.parse("SomeCommand | q=$var[4]! | arg1  @arg2.x $arg3[x]!.y![z] q -> k,v = $var[3]"), null, 2));