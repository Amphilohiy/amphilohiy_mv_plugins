var gulp = require('gulp');
var watch = require('gulp-watch');
var coffee = require('gulp-coffee');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var prettify = require('gulp-js-prettify');
var order = require('gulp-order');
var insert = require('gulp-insert');
var filter = require('gulp-filter');
var addsrc = require('gulp-add-src');

var fs = require('fs');
var errHandler = require('./error_handler');

var settings = JSON.parse(fs.readFileSync('./gulp/settings.json', 'utf8'));
var tasks = {
    build: ['prepeareLicense'],
    watch: ['prepeareLicense'],
    task: ['prepeareLicense']
};

var descriptionTransform = function(contents, file){
    locale = file.relative.split('/').pop().split('.')[0].replace('_', '');
    contents = contents.split("\n").map(function(line){
        return ' * ' + line;
    });
    return "###:" + locale + "\n" + contents.join("\n") + "\n ###";
};

var fileOrder = function(src){
    return [
        'templates/LICENSE',
        src + '/descriptions/*.txt',
        src + '/**/*.coffee',
        src + '/init.coffee'
    ]
};

var makeSrcDir = function(globs, src){
    var result = [];
    for (var i = 0; i < globs.length; i++){
        result.push(src + globs[i] + '.*')
    }
    return result;
};

var orderPreConcat = function(settings){
    var resultedOrder = [
        'templates/LICENSE',
        settings.src + 'descriptions/*.txt'
    ];
    resultedOrder = resultedOrder.concat(makeSrcDir(settings.order, settings.src));
    resultedOrder.push(settings.src + '**.*');
    return resultedOrder;
};

var piping = function(entrance, taskSettings, taskName){
    const f = filter([taskSettings.src + '/descriptions/*.txt'], {restore: true});

    if (taskSettings.customTasks){
        gulp.start(taskSettings.customTasks);
    }
    var preOrdered = entrance
        .pipe(f)
        .pipe(insert.transform(descriptionTransform))
        .pipe(f.restore);
        // .pipe(order(fileOrder(taskSettings.src)))
    if (taskSettings.order) {
        preOrdered = preOrdered.pipe(order(orderPreConcat(taskSettings)))
    }
    preOrdered = preOrdered
        .pipe(concat(taskName + '.js'))
        .pipe(coffee({bare: true}))
        .pipe(prettify());
    if (taskSettings.appendFiles){
    preOrdered = preOrdered
        .pipe(addsrc.append(makeSrcDir(taskSettings.appendFiles, taskSettings.src)))
        .pipe(concat(taskName + '.js'));
    }
    if (typeof taskSettings.dest === 'object'){
        for (var i in taskSettings.dest){
            preOrdered = preOrdered
                .pipe(gulp.dest(taskSettings.dest[i]))
        }
    } else {
        preOrdered
            .pipe(gulp.dest(taskSettings.dest));
    }
};

for (var taskName in settings) {
    var buildName = taskName + ':Build';
    var watchName = taskName + ':Watch';
    var pipeBuild = function(){
        var name = taskName;
        var taskSettings = settings[name];
        return function(){
            piping(gulp.src(fileOrder(taskSettings.src), {base: '.'}), taskSettings, name)
        };
    }();

    var pipeWatch = function(){
        var build = buildName;
        var name = taskName;
        var taskSettings = settings[name];
        return function(){
            watch(fileOrder(taskSettings.src), {read: false}, function(){
                piping(gulp.src(fileOrder(taskSettings.src), {base: '.'}).pipe(plumber({errorHandler: errHandler})), taskSettings, name)
            })
        };
    }();
    gulp.task(buildName, pipeBuild);
    gulp.task(watchName, pipeWatch);
    gulp.task(taskName, [buildName, watchName]);
    tasks.build.push(buildName);
    tasks.watch.push(watchName);
    tasks.task.push(taskName);
}

module.exports = tasks;