var libnotify = require( 'freedesktop-notifications' );
var gutil = require('gulp-util');
var gCrashSound = require('gulp-crash-wav-sound');

gCrashSound.config({
    file: __dirname + '/onerr.wav',
    duration: 3
});

var escape = function(string){
    var escapeTable = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;'
    };

    for (var char in escapeTable){
        var replacement = escapeTable[char];
        string = string.replace(char, replacement);
    }
    return string;
};

var errHandler = function(err){
    var line = err.code.split('\n')[err.location.first_line];
    var preErrLine = line.substring(0, err.location.first_column);
    var errLine = line.substring(err.location.first_column, err.location.last_column+1);
    var postErrLine = line.substring(err.location.last_column+1);

    libnotify.createNotification( {
        summary: 'error ' + err.plugin + ': ' + err.filename,
        body:
        escape(err.name + ': ' + err.message + "\n" + (err.location.first_line+1) + ':' + (err.location.first_column+1) + '  ' + preErrLine) + '<u><b>' +
        escape(errLine) + '</b></u>' +
        escape(postErrLine) + "\n",
        iconPath: 'appointment-new',
        timeout: 35000
    } ).push() ;

    console.log(err.toString());

    // throw err;
};

module.exports = gCrashSound.plumb(errHandler);