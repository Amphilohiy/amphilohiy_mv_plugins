esprima = require 'esprima'
read = require 'fs-readdir-recursive'
fs = require 'fs'

_ = require 'lodash'

mvPath = 'mv'
mvOut  = 'mv_doc'

files = read mvPath

ignoreList = [
  'libs/fpsmeter.js',
  'libs/iphone-inline-video.browser.js',
  'libs/lz-string.js',
  'libs/pixi-picture.js',
  'libs/pixi-tilemap.js',
  'libs/pixi.js',
  'main.js'
]

_.remove files, (file)->
  _.includes(ignoreList, file)

class BaseAnalyzer
  lexemsTypes: []
  lexemsValues: []

  constructor: (@classes, @filename)->
    @argsState = null

  lexemsSize: ->
    @lexemsTypes.length

  check: (stack)->
    # @TODO move to parser
    if stack.length < @lexemsTypes.length
      return false
    # check lexems types to ensure structure
    if !_.isEqual(_.map(stack, 'type'), @lexemsTypes)
      return false
    # check if capitalized (i.e. "constant")
    firstLetter = stack[0].value[0]
    return false unless firstLetter == firstLetter.toUpperCase() && firstLetter != '_' & firstLetter != '$'
    # check for full match
    for idx in @lexemsTypes
      lexem = @lexemsTypes[idx]
      if @lexemsValues[idx]?
        return false if lexem != @lexemsValues[idx]

    true

  actiavte: (stack)->
    raise 'Not implemented'

  setMethod: (constantName, methodName, loc)->
    @classes[constantName] ||= {
      methods: {},
      filename: @filename
      type: @type
    }
    @classes[constantName].methods[methodName] ||= {
      args: []
      loc: loc
    }
    @method = @classes[constantName].methods[methodName]

  argsParse: (token)->
    # if parenthesis is closed then method is finished
    if token.type == 'Punctuator' && token.value == ')'
      return false

    # argument name
    if !@argsState? && token.type == 'Identifier'
      @argsState = token.value
      @method.args.push(@argsState)
      return true

    # next method
    if token.type == 'Punctuator' && token.value == ','
      @argsState = null
    true

class ClassAnalyzer extends BaseAnalyzer
  lexemsTypes: [
    'Identifier' 
    'Punctuator' 
    'Identifier' 
    'Punctuator' 
    'Identifier' 
    'Punctuator' 
    'Keyword'    
  ]

  lexemsValues: [
    null
    '.'
    'prototype'
    '.'
    null
    '='
    'function'
  ]

  type: 'class'

  check: (stack)->
    # if we identifier "prototype" then we probably hit class
    if stack[2].value == "prototype" && stack[2].type == 'Identifier'
      return super(stack)
    false

  actiavte: (stack)->
    @setMethod(stack[0].value, stack[4].value, _.last(stack).loc)

class ModuleAnalyzer extends BaseAnalyzer
  lexemsTypes: [
    'Identifier' 
    'Punctuator'
    'Identifier' 
    'Punctuator' 
    'Keyword'    
  ]

  lexemsValues: [
    null
    '.'
    null
    '='
    'function'
  ]

  type: 'module'

  actiavte: (stack)->
    @setMethod(stack[0].value, stack[2].value, _.last(stack).loc)

class ClassParser
  constructor: (@classes, @filename)->
    @stack = []
    @currentAnalyzer = null
    @analyzers = [new ClassAnalyzer(@classes, @filename), new ModuleAnalyzer(@classes, @filename)]

  _check: ->
    _.each @analyzers, (analyzer)=>
      if analyzer.lexemsSize() <= @stack.length
        stackChunk = @stack.slice(@stack.length - analyzer.lexemsSize())
        if analyzer.check(stackChunk)
          @currentAnalyzer = analyzer
          analyzer.actiavte(stackChunk)

  process: (token)=>
    if !@currentAnalyzer?
      @stack.push(token)
      @_check()
    else
      unless @currentAnalyzer.argsParse(token)
        @stack = []
        @currentAnalyzer = null


parseFile = (filename, classes)->
  content = fs.readFileSync("#{mvPath}/#{filename}", 'utf8')
  lexems = esprima.parseScript(content, comment: true, range: true, loc: true)
  tokens = esprima.tokenize(content, comment: true, range: true, loc: true)

  parser = new ClassParser(classes, filename)
  _.each tokens, (token)->
    parser.process(token)

  fs.writeFile('.lexems.json', JSON.stringify(lexems, null, 2))
  fs.writeFile('.tokens.json', JSON.stringify(tokens, null, 2))
  parser.classes

dumpClass = (klass, className)->
  console.log("Dumping class: #{className}")
  doc = ["#{klass.type} #{className} // file: #{klass.filename}"]
  _.each klass.methods, (meta, method)-> 
    loc = "#{meta.loc.start.line}:#{meta.loc.start.column}"
    doc.push "#{_.padEnd(loc, 10)} #{method}(#{meta.args.join(', ')})"
  fs.writeFileSync "#{mvOut}/#{className}.txt", doc.join("\n")

classes = _.reduce files, (classes, filename)->
  console.log("Parsing '#{filename}'")
  parseFile(filename, classes)
, {}

files = read mvOut
_.each files, (filename)->
  fs.unlinkSync "#{mvOut}/#{filename}"

_.each classes, dumpClass
# console.log(JSON.stringify(classes, null, 2))