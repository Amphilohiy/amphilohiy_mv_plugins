PluginCommand
  = command:Name scope:(_ ScopeStatement)? args:(_ Arg)* postcall:(_ PostCallStatement)?

Arg
  = GlobalObject
  / StorageObject
  / Name

ObjectArg
  = GlobalObject
  / StorageObject
  / Variable

StorageObject
  = "@" ObjectExpression

GlobalObject
  = "$" ObjectExpression

ObjectExpression
  = head:Variable tail:(ObjectPath)*

ObjectPath
  = "." head:Variable unwind:"!"? tail:(ObjectPath)*
  / "[" head:ObjectArg "]" unwind:"!"? tail:(ObjectPath)*

ScopeStatement
  = '|' (_ Assignment)+ _ '|'

PostCallStatement
  = '->' (_ Assignment)+

Assignment
  = value:ObjectArg _ key:("," _ ObjectArg _)? "=" _ object:ObjectArg

_ "whitespace"
  = [ \t\n\r]*

// escape on special characters
Name
  = characters: [^ ]+ { return characters.join('') }

// escape on special characters
Variable
  = characters: [^ \t\n\r\.\[\]\(\)\{\}!=,]+ { return characters.join('') }