### register plugin manager commands ###
Amphicore.addRouterCommand(PluginManager.parameters('EventFactory')['Create event command'], EventFactory.createEvent, EventFactory)
Amphicore.addRouterCommand(PluginManager.parameters('EventFactory')['Remove event command'], EventFactory.removeEvent, EventFactory)
Amphicore.addRouterCommand(PluginManager.parameters('EventFactory')['Replace event command'], EventFactory.replaceEvent, EventFactory)

### prepeare all templates ###
EventFactory.loadTemplates()