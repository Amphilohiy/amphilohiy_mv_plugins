### Some unnecessary stuff ###
Imported = Imported || {}
Imported.EventFactory = 0.1;

### HORSE CORE ###
class Amphicore
  @pluginRoutes: {}

  ### for preloading purposes only. If you want suggest better async solution - i would gladly hear (maybe) ###
  @loadSync: (resource)->
    data = null
    xhr = new XMLHttpRequest()
    url = resource
    xhr.open('GET', url, false)
    xhr.overrideMimeType('application/json')
    xhr.onload = ->
      if (xhr.status < 400)
        data = JSON.parse(xhr.responseText)
    xhr.onerror = this._mapLoader || ->
      DataManager._errorUrl = DataManager._errorUrl || url;
    xhr.send()
    data

  ### adds command to plugin commands ###
  @addRouterCommand: (route, callback, bind)->
    command = new @PluginCommand(route, callback, bind)
    words = route.split(' ')
    lastWord = words.pop()
    node = @pluginRoutes
    for word in words
      node[word] ||= {}
      node = node[word]

    node[lastWord] = command

  ### executes plugin command ###
  @executePluginCommand: (command, args)->
    fullRequest = [command].concat(args)
    remainArgs = JsonEx.makeDeepCopy(fullRequest)

    node = @pluginRoutes
    for word in fullRequest
      if node[word]
        node = node[word]
        remainArgs.shift()
        if node instanceof Amphicore.PluginCommand
          node.invoke(remainArgs)
          return true
      else
        return false
    false

### class for registered plugin commands ###
class Amphicore.PluginCommand
  constructor: (route, @callback, @bind)->
    @route = route.toUpperCase()

  invoke: (args)->
    @callback.apply(@bind || @command, args)