### dumping commands on saving game ###
DataManager._eventFactoryMakeSaveContents = DataManager.makeSaveContents
DataManager.makeSaveContents = ->
  contents = DataManager._eventFactoryMakeSaveContents()
  contents.eventFactory = $gameEventFactory
  return contents

### loading commands on loading game ###
DataManager._eventFactoryExtractSaveContents = DataManager.extractSaveContents
DataManager.extractSaveContents = (contents)->
  DataManager._eventFactoryExtractSaveContents(contents)
  window.$gameEventFactory = contents.eventFactory

### creating empty commands on new game ###
DataManager._eventFactoryCreateGameObjects = DataManager.createGameObjects
DataManager.createGameObjects = ->
  @_eventFactoryCreateGameObjects()
  window.$gameEventFactory = {}

### injecting events from factory on loading map ###
DataManager._eventFactoryOnLoad = DataManager.onLoad
DataManager.onLoad = (object)->
  ### check if we loaded map ###
  if `object === $dataMap`
    sceneMap = EventFactory.getSceneMap()
    ### this gross thing stealed from SceneManager ###
    mapId = if sceneMap && sceneMap._transfer then $gamePlayer.newMapId() else $gameMap.mapId()
    if mapId > 0
      ### actual injecting ###
      commands = EventFactory.getFactoryEvents(mapId)
      if commands?
        for _, command of commands
          command.execute(object.events)
  @_eventFactoryOnLoad(object)

### method to apply command on current map ###
Game_Map.prototype.applyFactoryCommand = (command)->
  command.executeCurrentMap(@)
  # @TODO mechanism to refresh once per frame
  @refreshTileEvents()

Game_Interpreter.prototype._amphicorePluginCommand = Game_Interpreter.prototype.pluginCommand
Game_Interpreter.prototype.pluginCommand = (command, args)->
  ### if routing fails fallback to original "routing" ###
  unless Amphicore.executePluginCommand(command, args)
    @_amphicorePluginCommand(command, args)