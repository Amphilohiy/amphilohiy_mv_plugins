  # BTW if you think why da heck there is one more indentation - this is continuation of event factory module
  ### function to create event ###
  @createEvent: (name, x, y, mapId, variableId)->
    x = Number(x)
    y = Number(y)
    if mapId
      mapId = Number(mapId)
    else
      mapId = $gameMap.mapId()
    unless variableId
      variableId = @varId
    variableId = Number(variableId)

    events = @getFactoryEvents(mapId)
    freeId = @getFreeId(mapId)
    command = new CreateFactoryCommand(name, freeId, x, y)
    events[freeId] = command
    if variableId
      $gameVariables.setValue(variableId, freeId)

    if mapId == $gameMap.mapId()
      $gameMap.applyFactoryCommand(command)

    freeId

  ### function to destroy event ###
  @removeEvent: (id, mapId)->
    if id
      id = Number(id)
    else
      id = $gameVariables.value(@varId)
    if mapId
      mapId = Number(mapId)
    else
      mapId = $gameMap.mapId()

    mapId ||= $gameMap.mapId()
    events = @getFactoryEvents(mapId)
    command = new RemoveFactoryCommand(id)
    ### in case original event existed - keep remove command, otherwise we don't need command ###
    if @getEventIndexes(mapId).indexOf(id) != -1
      events[id] = command
    else
      delete events[id]

    if mapId == $gameMap.mapId()
      $gameMap.applyFactoryCommand(command)

  ### function to replace event ###
  @replaceEvent: (name, id, x, y, mapId)->
    if id
      id = Number(id)
    else
      id = $gameVariables.value(@varId)
    if x
      x = Number(x)
    if y
      y = Number(y)
    if mapId
      mapId = Number(mapId)
    else
      mapId = $gameMap.mapId()

    events = @getFactoryEvents(mapId)
    command = new ReplaceFactoryCommand(name, id, x, y)

    ### in case original event existed - keep replace command, otherwise we just store creation command ###
    if @getEventIndexes(mapId).indexOf(id) != -1
      events[id] = command
    else
      events[id] = new CreateFactoryCommand(name, id, events[id].x, events[id].y)

    if mapId == $gameMap.mapId()
      $gameMap.applyFactoryCommand(command)