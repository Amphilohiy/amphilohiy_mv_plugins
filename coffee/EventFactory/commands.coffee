###
  Command pattern which apply changes to map. Serializable.
  All commands represent difference between original map and modified.
###
### Basic command ###
class CommonFactoryCommand
  ### get type of command ###
  type: ->
  ### makes deep copy of event and apply new parameters to it ###
  extendEvent: (opts)->
    opts ||= {}
    event = JsonEx.makeDeepCopy(EventFactory.factoryTemplates[@name])
    for key, value of opts
      event[key] = value
    event

  ### callback which runs on map loading ###
  execute: (events)->

  ###
  callback which runs on current map
  primarily tries to work with spritesets
  ###
  executeCurrentMap: (gameMap)->

### Command for putting new events ###
class CreateFactoryCommand extends CommonFactoryCommand
  constructor: (@name, @id, @x, @y)->

  type: ->
    'create'

  execute: (events)->
    events[@id] = @extendEvent
      id: @id
      x: @x,
      y: @y

  executeCurrentMap: (gameMap)->
    @execute($dataMap.events)
    newEvent = new Game_Event($gameMap.mapId(), @id)
    gameMap._events[@id] = newEvent
    sceneMap = EventFactory.getSceneMap()
    unless sceneMap
      console.log('Map\'s scene not found! This might be an error.')
      return

    eventSprite = new Sprite_Character(newEvent)

    sceneMap._spriteset._characterSprites.push(eventSprite)
    sceneMap._spriteset._tilemap.addChild(eventSprite)


### Command for deleting events ###
class RemoveFactoryCommand extends CommonFactoryCommand
  constructor: (@id)->

  type: ->
    'remove'

  execute: (events)->
    delete events[@id]

  executeCurrentMap: (gameMap)->
    oldEvent = gameMap._events[@id]
    delete gameMap._events[@id]

    sceneMap = EventFactory.getSceneMap()
    unless sceneMap
      console.log('Map\'s scene not found! This might be an error.')
      return

    unless oldEvent
      return

    sprites = sceneMap._spriteset._characterSprites
    for idx, sprite of sprites
      if sprite._character._eventId == oldEvent._eventId
        delete sceneMap._spriteset._characterSprites[idx]
        sceneMap._spriteset._tilemap.removeChild(sprite)


### Command for replacing events ###
class ReplaceFactoryCommand extends CommonFactoryCommand
  constructor: (@name, @id, @x, @y)->

  type: ->
    'replace'

  execute: (events)->
    originalEvent = events[@id]
    ### @TODO decide if i want to save original position or current ###
    @x ||= originalEvent.x unless @x
    @y ||= originalEvent.y unless @y
    events[@id] = @extendEvent
      id: @id
      x: @x,
      y: @y

  executeCurrentMap: (gameMap)->
    oldEvent = gameMap._events[@id]

    sceneMap = EventFactory.getSceneMap()
    unless sceneMap
      console.log('Map\'s scene not found! This might be an error.')
      return

    sprites = sceneMap._spriteset._characterSprites
    for idx, sprite of sprites
      if sprite._character._eventId == oldEvent._eventId
        delete sceneMap._spriteset._characterSprites[idx]
        sceneMap._spriteset._tilemap.removeChild(sprite)

    $dataMap.events[@id] = @extendEvent
      id: @id
      x: oldEvent.x,
      y: oldEvent.y

    newEvent = new Game_Event($gameMap.mapId(), @id)
    gameMap._events[@id] = newEvent
    eventSprite = new Sprite_Character(newEvent)

    sceneMap._spriteset._characterSprites.push(eventSprite);
    sceneMap._spriteset._tilemap.addChild(eventSprite);

class NothingFactoryCommand extends CommonFactoryCommand