### Events factory module contains all basic functions ###
class EventFactory
  @factoryTemplates: {}
  @eventFactoryIndexes: {}
  @varId: PluginManager.parameters('EventFactory')['Default id variable']

  ### preload all events to cache ###
  @loadTemplates: ->
    templates = PluginManager.parameters('EventFactory')['Templates']
    templates = templates.substring(1, templates.length-1).replace(/"/g, '').split(',');
    for template in templates
      @loadTemplate(template*1)

  ### preload all events to cache from specific map ###
  @loadTemplate: (mapId)->
    mapId = 'data/Map%1.json'.format(mapId.padZero(3))
    map = Amphicore.loadSync(mapId)
    for event in map.events
      if event?
        @factoryTemplates[event.name] = event

  ### gets original event mapping, which would help us to create commands. Cached. ###
  @getEventIndexes = (mapId)->
    indexes = @eventFactoryIndexes[mapId]
    unless indexes?
      return @eventFactoryIndexes[mapId] = @stripIndexes(Amphicore.loadSync('data/Map%1.json'.format(mapId.padZero(3))))
    indexes

  ### gets mapping from specific map ###
  @stripIndexes = (map)->
    indexes = []
    for event in map.events
      if event?
        indexes.push(event.id)
    indexes

  ### gets commands mapping for map. Cached. ###
  @getFactoryEvents = (mapId)->
    events = window.$gameEventFactory[mapId]
    unless events?
      events = {}
      window.$gameEventFactory[mapId] = events
      indexes = @getEventIndexes(mapId)
      ### placing empty factory commands to simplify search for free ids ###
      for index in indexes
        events[index] = new NothingFactoryCommand()
    events

  ### seek for free id on map ###
  @getFreeId = (mapId)->
    events = @getFactoryEvents(mapId)
    currentId = 1
    while true
      ### if there is no original event or remove command ###
      unless (events[currentId]? && events[currentId].type != 'remove') || @getEventIndexes[currentId]
        return currentId
      currentId++

  ### searching for map's scene ###
  @getSceneMap: ->
    ### @TODO: check parse stack ###
    if SceneManager._scene instanceof Scene_Map
      return SceneManager._scene

    for scene in SceneManager._stack
      if scene instanceof Scene_Map
        return scene
    null
